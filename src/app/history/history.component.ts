import { Component, OnInit } from '@angular/core';
import { HistoryService } from '../history.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  sentnotifications: any = [];
  statusMessage:String= '';
  constructor(private historyService: HistoryService, private toastr: ToastrService) { }

  ngOnInit() {
    this.getAllSentNotifications();
  }
  getAllSentNotifications() {
    this.historyService.getAllSentNotifications()
      .subscribe(res => {
        if (res.status.code != 200) {
          this.toastr.error(res.status.message);

        }
        else {
          if (res.data.length == 0) {
            this.statusMessage = 'No Notifications Sent yet'
          }
          else {
            this.sentnotifications = res.data;
            console.log(this.sentnotifications);
          }

        }
      }, err => {
        this.toastr.error('Web services Are not running , Please check the server');
        console.log(err);
      });
  }
}

import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { tap, catchError, map} from 'rxjs/operators';
import { CommonMethods, EndPointURL } from './utilities/constants';


@Injectable({
  providedIn: 'root'
})
export class ViewMessagesService {

  constructor(private http: HttpClient) { }
 

  getAllNotifications (): Observable<any> {
    const endpoint = CommonMethods.GetServiceURL(EndPointURL.GetMessageList);
    return this.http.get(endpoint)
      .pipe(
        tap(data => console.log('fetched products')),
       catchError(this.handleError)
      );
  }

  
  deleteNotification(notificationObj): Observable<any> {
    const endpoint = CommonMethods.GetServiceURL(EndPointURL.DeleteMessage);
    let body: any = {
      "id": notificationObj.id
    }
    return this.http.post(endpoint, body);
  }

  getDetails (notificationObj): Observable<any> {
    const endpoint = CommonMethods.GetServiceURL(EndPointURL.GetDetail);
    let body: any = {
      "id": notificationObj.id
    }
    return this.http.post(endpoint, body);
  }

  editDetails (notificationObj): Observable<any> {
    const endpoint = CommonMethods.GetServiceURL(EndPointURL.EditMessage);
    let body: any = {
      "id": notificationObj.id,
      "message":notificationObj.message ,
      "title" : notificationObj.title 
    }
    return this.http.post(endpoint, body);
  }
  sendNotification(notificationObj): Observable<any> {
    const endpoint = CommonMethods.GetServiceURL(EndPointURL.Send);
    let body: any = {
      "id": notificationObj.id,
      "message":notificationObj.message ,
      "title" : notificationObj.title 
    }
    return this.http.post(endpoint, body);
  }

  private handleError(error: any) {
                       //Created a function to handle and log errors, in case
    return throwError(error);
  }
}

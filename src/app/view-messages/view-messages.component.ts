import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { isNullOrUndefined } from 'util';
import { ViewMessagesService } from '../view-messages.service';

@Component({
  selector: 'app-view-messages',
  templateUrl: './view-messages.component.html',
  styleUrls: ['./view-messages.component.css']
})
export class ViewMessagesComponent implements OnInit {
  form: FormGroup;
  notificationRes = {

  };
  viewNotification = {
    message: '',
    id: 0,
    title: ''
  };
  deletedisplay = 'none';
  display = 'none';
  senddisplay = 'none';
  notifications: any = [];
  statusMessage = '';
  constructor(private viewMessagesService: ViewMessagesService, private toastr: ToastrService) { }
  ngOnInit() {
    this.getAllNotifications();
  }

  getAllNotifications() {

    this.viewMessagesService.getAllNotifications()
      .subscribe(res => {
        if(res.status.code != 200) 
        {
          this.toastr.error(res.status.message);
         
        }
        else{
          if(res.data.length == 0) {
            this.statusMessage = 'No Messages added yet'
          }
          this.notifications = res.data;
          console.log(this.notifications);
          // this.isLoadingResults = false;
        }
       
      }, err => {
        this.toastr.error('Web services Are not running , Please check the server');
        console.log(err);
      } );
  }
  delete() {
    this.viewMessagesService.deleteNotification(this.viewNotification)
      .subscribe(res => {
        if(res.status.code != 200) 
        {
        this.toastr.error(res.status.message);
         
        }
        else{
          this.toastr.success('Deleted Sucessfully');
          this.onCloseHandledDeletePopup();
          this.getAllNotifications();
        }
        
      }, err => {
        this.toastr.error('Web services Are not running , Please check the server');
        console.log(err);
      } );
  }
  GetDetails(notification: any) {
    this.viewMessagesService.getDetails(notification)
      .subscribe(res => {
        if(res.status.code != 200) 
        {
        this.toastr.error(res.status.message);
         
        }
        else{ 
          this.viewNotification.id = res.notification.id;
          this.viewNotification.message = res.notification.message;
          this.viewNotification.title = res.notification.title;
        }
     

      }, err => {
        this.toastr.error('Web services Are not running , Please check the server');
        console.log(err);
      } );
  }
  editSubmit(regForm: NgForm) {
    let notificationObj: any = {
      id: this.viewNotification.id,
      message: regForm.value.message,
      title: regForm.value.title
    }

    this.viewMessagesService.editDetails(notificationObj)
      .subscribe((data: any) => {
        if(data.status.code != 200) 
        {
        this.toastr.error(data.status.message);
         
        }
        else{ 
          console.log(data);
          this.toastr.success('Notification Updated sucessfully');
          this.onCloseHandled();
          this.getAllNotifications();
        }
      
      } , err => {
        this.toastr.error('Web services Are not running , Please check the server');
        console.log(err);
      } )
  }
  onCloseHandled() {
    this.display = 'none'; //set none css after close dialog
  }
  onCloseHandledDeletePopup() {
    this.deletedisplay = 'none'; //set none css after close dialog
  }
  onCloseSend() {
    this.senddisplay = 'none'; //set none css after close dialog
  }
  openModalAndGetDetails(notification: any) {
    this.display = 'block'; //Set block css
    this.viewMessagesService.getDetails(notification)
      .subscribe(res => {
        if(res.status.code != 200) 
        {
        this.toastr.error(res.status.message);
         
        }
        else{ this.viewNotification.id = res.data.id;
          this.viewNotification.message = res.data.message;
          this.viewNotification.title = res.data.title;}
        
      }, err => {
        this.toastr.error('Web services Are not running , Please check the server');
        console.log(err);

      });
  }
  openDeleteModalAndGetDetails(notification: any) {
    this.deletedisplay = 'block';//set none css after close dialog
    this.viewMessagesService.getDetails(notification)
      .subscribe(res => {
        this.viewNotification.id = res.data.id;
        this.viewNotification.message = res.data.message;
        this.viewNotification.title = res.data.title;
      }, err => {
        this.toastr.error('Web services Are not running , Please check the server');
        console.log(err);
        //  this.isLoadingResults = false;
      });
  }
  openModalForSendNotification(notification) {
    this.senddisplay = 'block'; //Set block css
    this.viewMessagesService.getDetails(notification)
      .subscribe(res => {
        if(res.status.code != 200) 
        {
        this.toastr.error(res.status.message);
         
        }
        else{this.viewNotification.id = res.data.id;
          this.viewNotification.message = res.data.message;
          this.viewNotification.title = res.data.title; 
        }
        
      }, err => {
        this.toastr.error('Web services Are not running , Please check the server');  
        console.log(err);
      });
  }
  sendNotification(regForm: NgForm) {
    let notificationBody = {
      id: this.viewNotification.id,
      message: regForm.value.message,
      title: regForm.value.title
    }
    this.viewMessagesService.sendNotification(notificationBody)
      .subscribe(res => {
         let resp = res.data;
         this.toastr.success(res.status.message);
         regForm.reset();
         this.senddisplay = 'none'; 

      }, err => {
        this.toastr.error('Web services Are not running , Please check the server');
        console.log(err);
      });
  }
}

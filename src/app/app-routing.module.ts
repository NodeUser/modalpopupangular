import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistoryComponent } from './history/history.component';
import { ViewMessagesComponent } from './view-messages/view-messages.component';
import { AddMessageComponent } from './add-message/add-message.component';


const routes: Routes = [

  {path:  'add', component: AddMessageComponent },
  {path:  'view', component:ViewMessagesComponent },
  {path:  'history', component:HistoryComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

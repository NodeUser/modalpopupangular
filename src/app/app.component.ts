import { Component } from '@angular/core';

import { SwPush } from '@angular/service-worker';
import { PushNotificationService } from './push-notification.service';




const VAPID_PUBLIC =
  'BBb96CrbZ9D8IYvFByDc51_SaMfEKPnsYFi_U61KbBZWM1VPvb_E6qI3IWvar8XEqrqQDA3epN-U5eW5paWS7_Q';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PwaSails';
  constructor(swPush: SwPush, pushService: PushNotificationService) {
    if (swPush.isEnabled) {
      swPush
        .requestSubscription({
          serverPublicKey: VAPID_PUBLIC,
        })
        .then(subscription => {
          console.log(subscription);
          pushService.sendSubscriptionToTheServer(subscription).subscribe();
        })
        .catch(console.error);
    }
  }
}

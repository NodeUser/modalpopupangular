import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { PushNotificationService } from './push-notification.service';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 // import this new file
import { MaterialModule } from './material';
import { ToastrModule } from 'ngx-toastr';

import { HeaderComponent } from './header/header.component';

import { ViewMessagesComponent } from './view-messages/view-messages.component';
import { AddMessageComponent } from './add-message/add-message.component';
import { HistoryComponent } from './history/history.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ViewMessagesComponent,
    AddMessageComponent,
    HistoryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule ,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot() ,// ToastrModule added
    ServiceWorkerModule.register('/ngsw-worker.js', {
      enabled: environment.production,
    })
  ],
  providers: [PushNotificationService],
  bootstrap: [AppComponent]
})
export class AppModule { }

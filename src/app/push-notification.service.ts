import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';

import { Observable } from 'rxjs';
const SERVER_URL = 'http://localhost:3000/subscription';
const apiURL = 'http://localhost:3000/sendNotification';
@Injectable({
  providedIn: 'root'
})
export class PushNotificationService {

  constructor(private http: HttpClient) { }

  public sendSubscriptionToTheServer(subscription: PushSubscription) {
    return this.http.post(SERVER_URL, subscription);
  }

   sendNotifications(notificationObj): Observable<any> {
    let body: any = {
      //"title": notificationObj.title,
      "message": notificationObj.message
    }
  return this.http.post(apiURL,body );
  }
}

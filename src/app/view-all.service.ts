import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { CommonMethods, EndPointURL } from './utilities/constants';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ViewAllService {

  constructor(private _http: HttpClient) { }


  GetAllMessagesList(): Observable<any> {

    const endpoint = CommonMethods.GetServiceURL(EndPointURL.GetMessageList);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      })
    };

    return this._http
      .get(endpoint, httpOptions)
  }


  GetMessageDetail(notification): Observable<any> {
    const endpoint = CommonMethods.GetServiceURL(EndPointURL.GetMessageDetail);
    const body = {
      "id": notification.ids
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      })
    };

    return this._http
      .post(endpoint, body, httpOptions);
  }
  sendNotifications(notification): Observable<any> {
    const endpoint = CommonMethods.GetServiceURL(EndPointURL.GetMessageDetail);
    const body = {
      "message": notification.message
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      })
    };

    return this._http
      .post(endpoint, body, httpOptions);
  }
  HandleObservableError(errorResponse: HttpErrorResponse) {

    console.log('CS ERROR ' + errorResponse.error.message);

    if (errorResponse.status === 403) {
      localStorage.removeItem('authenticate');
      // window.open('../#/login', '_self');
    }
    return throwError(errorResponse);
  }

}

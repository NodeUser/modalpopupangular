import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { tap, catchError, map } from 'rxjs/operators';
import { CommonMethods, EndPointURL } from './utilities/constants';


@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  constructor(private http: HttpClient) { }

  getAllSentNotifications(): Observable<any> {
    const endpoint = CommonMethods.GetServiceURL(EndPointURL.GetMessageList);
    return this.http.get(endpoint)
      .pipe(
        tap(data => console.log('fetched products')),
        catchError(this.handleError)
      );
  }
  private handleError(error: any) {
    //Created a function to handle and log errors, in case
    return throwError(error);
  }
}

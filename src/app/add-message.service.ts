import { Injectable } from '@angular/core';
import { CommonMethods, EndPointURL } from './utilities/constants';

import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { tap, catchError, map} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AddMessageService {

  constructor(private http: HttpClient) { }
   
  saveNotifications(notificationObj): Observable<any> {
    const endpoint = CommonMethods.GetServiceURL(EndPointURL.AddMessage);
    let body: any = {
      "title" : notificationObj.title ,
      "message": notificationObj.message
    }
  
    return this.http.post(endpoint, body);
  }
  sendAndSave(notificationObj): Observable<any> {
    const endpoint = CommonMethods.GetServiceURL(EndPointURL.SaveAndSend);
    let body: any = {
      "title" : notificationObj.title ,
      "message": notificationObj.message
    }
  
    return this.http.post(endpoint, body);
  }
}

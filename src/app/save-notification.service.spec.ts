import { TestBed } from '@angular/core/testing';

import { SaveNotificationService } from './save-notification.service';

describe('SaveNotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaveNotificationService = TestBed.get(SaveNotificationService);
    expect(service).toBeTruthy();
  });
});

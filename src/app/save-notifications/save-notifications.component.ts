import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-save-notifications',
  templateUrl: './save-notifications.component.html',
  styleUrls: ['./save-notifications.component.css']
})
export class SaveNotificationsComponent implements OnInit {
  display='none';
  constructor() { }

  ngOnInit() {

  }

  openModal(){
    this.display='block'; //Set block css
 }

 getAllUsers(){
  
  //set none css after close dialog
 }
onCloseHandled(){
  this.display='none'; //set none css after close dialog
 }
 
}

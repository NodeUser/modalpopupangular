import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AddMessageService } from '../add-message.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-add-message',
  templateUrl: './add-message.component.html',
  styleUrls: ['./add-message.component.css']
})
export class AddMessageComponent implements OnInit {
  saveNotificationRequest = {
    title: '',
    message: ''
  };
  viewNotification = {
    title: '',
    message: ''
  };
  display = 'none';
  constructor(private toastr: ToastrService, private addMessageService: AddMessageService) { }
  ngOnInit() {
  }
  reset(regForm: NgForm) {
    regForm.reset();
  }
  openPopup(regForm: NgForm) {
    if (regForm.valid) {
      this.display = 'block';
      this.viewNotification.title = regForm.value.title;
      this.viewNotification.message = regForm.value.message;
    }
    else {
      this.toastr.error('Please enter the Valid Fields');
    }
  }
  saveSubmit(regForm: NgForm) {
    if (regForm.value.message == '' || isNullOrUndefined(regForm.value.message)) {
      this.toastr.error('Please Add a message');
    }
    if (regForm.value.title == '' || isNullOrUndefined(regForm.value.title)) {
      this.toastr.error('Please Add a title');
    }
    else {
      let notificationObj: any = {
        message: regForm.value.message,
        title: regForm.value.title,
      }

      this.addMessageService.saveNotifications(notificationObj)
        .subscribe((addResponse: any) => {
          if (addResponse.status.code != 200) {
            this.toastr.error(addResponse.status.message);

          }
          else {
            this.toastr.success(addResponse.status.message);
            regForm.reset();
            window.open('../view', '_self');
          }
        })

    }

  }
  sendNotification(regForm: NgForm) {
    if (regForm.value.message == '' || isNullOrUndefined(regForm.value.message)) {
      this.toastr.error('Please Add a message');
    }
    if (regForm.value.title == '' || isNullOrUndefined(regForm.value.title)) {
      this.toastr.error('Please Add a title');
    }
    else {
      let notificationObj: any = {
        message: regForm.value.message,
        title: regForm.value.title,
      }

      this.addMessageService.sendAndSave(notificationObj)
        .subscribe((addResponse: any) => {
          if (addResponse.status.code != 200) {
            this.toastr.error(addResponse.status.message);
          }
          else {
            this.toastr.success(addResponse.status.message);
            regForm.reset();
            this.onCloseHandled();
            window.open('../view', '_self');
          }


          // this.getAllNotifications();
        })

    }
  }
  onCloseHandled() {
    this.display = 'none';
  }
}

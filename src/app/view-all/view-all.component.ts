import { Component, OnInit } from '@angular/core';
import { ViewAllService } from '../view-all.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-view-all',
  templateUrl: './view-all.component.html',
  styleUrls: ['./view-all.component.css']
})
export class ViewAllComponent implements OnInit {
  messageList: any = [
    { id: '', title: 'Festiwal Notification', message: 'Happy Diwali' },
    { id: '', title: 'Festiwal Notification', message: 'Happy Diwali' },
    { id: '', title: 'Festiwal Notification', message: 'Happy Diwali' }];
  displayDeleteDialog = 'none'; // default Variable
  displayDetailDialog = 'none';
  displaysendDialog = 'none';
  setViewNotification: any = {
    title: '',
    message: '',
    id: ''
  }
  constructor(private viewAllService: ViewAllService) { }

  ngOnInit() {
    this.GetAllMessagesList();
  }
  GetAllMessagesList() {

    this.viewAllService.GetAllMessagesList().subscribe((data) => {


      //           let datas =   [
      //           { id: '', title: 'Festiwal Notification', message: 'Happy Diwali' },
      //           { id: '', title: 'Festiwal Notification', message: 'Happy Diwali' },
      //           { id: '', title: 'Festiwal Notification', message: 'Happy Diwali' }
      //  ];

      this.messageList = data;
      console.log(this.messageList);

    }, (errorResponse) => {

      // this.toastr.error(errorResponse.error.message, 'Error');

    });
  }
  CloseDeleteModalDialog() {
    this.displayDeleteDialog = 'none';
  }
  OpenDeleteModalDialog(notifications) {
    this.displayDeleteDialog = 'block';

  }
  CloseUpdateModalDialog() {
    this.displayDetailDialog = 'none';
  }
  CloseSendModalDialog() {
    this.displaysendDialog = 'none';
  }

  GetDetailOpenPopup(notification: any) {
    this.displayDetailDialog = 'block';
    let notificationRequest = {
      id: notification
    }
    this.viewAllService.GetMessageDetail(notificationRequest)
      .subscribe((resData) => {
        this.SetViewNotification(resData.data)
        console.log(this.messageList);

      }, (errorResponse) => {

      });

  }
  SetViewNotification(data: any) {
    this.setViewNotification.id = data.id;
    this.setViewNotification.title = data.title;
    this.setViewNotification.message = data.message;
  }

  GetDetailOpenSendPopup(notification: any)
{
  this.displaysendDialog = 'block';
  let notificationRequest = {
    id: notification
  }
  this.viewAllService.GetMessageDetail(notificationRequest).subscribe((resData) => {
      this.SetViewNotification(resData.data)
   
    }, (errorResponse) => {

    });
}

sendNotifications(formData: NgForm): void{
  if (formData.valid) {
    let sendNotificationsRequest = {
      message : formData.value.message
    }
    this.viewAllService.sendNotifications(sendNotificationsRequest)
    .subscribe((data) => {
      this.statusMessage = 'Password changed sucessfully';
      formData.reset();
      this.toastr.success('Password changed sucessfully', 'Success');
      setTimeout(() => { this.spinner.hide(); }, 500);
    }, (errorResponse) => {
      // if (errorResponse.error instanceof ErrorEvent) {
    this.toastr.error(errorResponse.error.message, 'Error');
    // } else {
      // this.toastr.error(errorResponse, 'Error');
    // }
      this.spinner.hide();
      });
  }
}
}

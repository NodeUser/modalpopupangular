
  export class EndPointURL {
   
    static readonly APIBaseUrl = 'http://localhost:3000/';

    static readonly GetMessageList = 'getList';
    static readonly GetDetail = 'detailMessage';
    static readonly AddMessage = 'add';
    static readonly EditMessage = 'editMessage';
    static readonly DeleteMessage = 'deleteMessage';
     static readonly SaveAndSend = 'sendAndSaveMessage';
     static readonly Send = 'send';
     static readonly history = 'history';
   
  }

  export class CommonMethods {
    public static GetServiceURL(serviceName: string): string {
      return  EndPointURL.APIBaseUrl + serviceName;
    }
}
